.ALIASES
V_V2            V2(+=N9110263 -=0 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110313@SOURCE.VPULSE.Normal(chips)
C_Cout          Cout(1=0 2=N9110045 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110189@ANALOG.C.Normal(chips)
C_Cs            Cs(1=N9110021 2=N9110033 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9109965@ANALOG.C.Normal(chips)
R_Ro            Ro(1=0 2=N9110045 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110151@ANALOG.R.Normal(chips)
R_Rp            Rp(1=N9110263 2=N9110141 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110221@ANALOG.R.Normal(chips)
L_L1            L1(1=N9110005 2=N9110021 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9109943@ANALOG.L.Normal(chips)
D_D             D(A=N9110033 C=N9110045 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9109981@IRF.10TQ045.Normal(chips)
V_V1            V1(+=N9110005 -=0 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110071@SOURCE.VDC.Normal(chips)
X_M             M(D=N9110021 G=N9110141 S=0 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110091@IRF.IRF1010N.Normal(chips)
C_Cin           Cin(1=0 2=N9110005 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110173@ANALOG.C.Normal(chips)
L_L2            L2(1=0 2=N9110033 ) CN @SEPIC_CONVERTER.SCHEMATIC1(sch_1):INS9110117@ANALOG.L.Normal(chips)
.ENDALIASES
